import React, { useEffect } from 'react'
import { View, Text, Image, Button, SafeAreaView, TouchableOpacity, TextInput,StyleSheet } from "react-native"
export default function Welcome() {
    return (
        <View style ={styles.container}>
        <View style ={styles.image}>
        <Image 
        style={{ height: 78, width: 169, marginLeft: 120, marginTop: 70}}
        source={require("./images/bgkukawa2.png/")}
        />
        <Text style ={{alignSelf: 'flex-start', marginLeft: 30, marginTop: 100, fontSize: 48, color: '#F6F1F1'}}>Welcome</Text>
        <View style={styles.header}>
            <View style={styles.reg}>
                <TouchableOpacity>
                <Text style={{alignSelf: 'center', color:'white', fontSize:28, marginTop:3}}>Register</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.log}>
            <Text style={{alignSelf: 'center', color:'white', fontSize:28, marginTop:3}}>Login</Text>
            </View>
            
                    </View>

        </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: '#B92F2F',
        opacity:0.96

    },

    header :{
        backgroundColor:'#D8D5D5',
        borderColor: '#000000', 
        borderTopLeftRadius:50, 
        borderTopRightRadius:50, 
        height: 250, width: 360, 
        marginTop: 170, 
        borderWidth: 1

    },
    reg:{
        backgroundColor:'#CC1A1A', 
        alignSelf:'center',
        borderColor: '#000000',
        borderTopRightRadius:10, 
        borderTopLeftRadius:10, 
        borderBottomLeftRadius:10, 
        borderBottomRightRadius:10, 
        height: 50, 
        width: 300, 
        marginTop: 70, 
        marginBottom:10, 
        borderWidth: 1
    },
    log:{
        backgroundColor:'#1A1818', 
        alignSelf:'center',
        borderColor: '#000000',
        borderTopRightRadius:10, 
        borderTopLeftRadius:10, 
        borderBottomLeftRadius:10, 
        borderBottomRightRadius:10, 
        height: 50, 
        width: 300, 
        borderWidth: 1
    }
})