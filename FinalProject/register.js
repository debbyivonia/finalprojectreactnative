import React, { useEffect } from 'react'
import { View, Text, Image, Button, SafeAreaView, TouchableOpacity, TextInput, StyleSheet } from "react-native"
export default function Register() {
    return (
        <View style={styles.container}>
            <View style={styles.image}>
                <Image
                    style={{ height: 78, width: 169, marginLeft: 120, marginTop: 70 }}
                    source={require("./images/bgkukawa2.png/")}
                />
                <Text style={{ alignSelf: 'flex-start', marginLeft: 30, marginTop: 100, fontSize: 48, color: '#F6F1F1' }}>Lets Start</Text>
                <View style={styles.header}>
                <Text style={{alignSelf:'flex-start', marginLeft:50, marginTop:20, fontSize:36 }}>Sign Up</Text>
                    <Text style={styles.text}>Name</Text>
                    <View style={styles.lineStyle} />
                    <Text style={styles.text}>Email</Text>
                    <View style={styles.lineStyle} />
                    <Text style={styles.text}>Phone Number</Text>
                    <View style={styles.lineStyle} />
                    <Text style={styles.text}>Password</Text>
                    <View style={styles.lineStyle} />
                    <View style={{ alignSelf: "flex-start", marginLeft: 20, flexDirection: 'row' }}>
                        <Text style={{ alignSelf: 'flex-start', marginTop: 30, fontSize: 18, color: '#C51D1D' }}>Already have an account ?</Text>
                        <Text style={{ alignSelf: 'flex-start', marginLeft: 50, marginTop: 30, fontSize: 18, color: 'black' }}>Sign Up</Text>

                    </View>




                </View>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#B92F2F',
        opacity: 0.96

    },

    header: {
        backgroundColor: '#D8D5D5',
        borderColor: '#000000',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        height: 400, width: 360,
        marginTop: 20,
        borderWidth: 1

    },

    lineStyle: {
        borderWidth: 0.5,
        borderColor: 'black',
        marginTop: 5,
        width: 250,
        marginLeft: 40
    },
    text: {
        marginLeft: 40,
        marginTop: 35,
        fontSize: 18,
        opacity: 0.50
    }

})