import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import Splash from '../FinalProject/splash.js';
import Welcome from '../FinalProject/welcome.js'
import Register from '../FinalProject/register.js'
import Login from '../FinalProject/login.js'
import Home from '../FinalProject/home.js'

export default function App() {
  return (
    <Home/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
