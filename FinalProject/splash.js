import React, { useEffect } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, TextInput,StyleSheet } from "react-native"
export default function Splash() {
    return (
        <View style ={styles.container}>
            <Image 
            style={{ height: 150, width: 250}}
            source={require("./images/KukawaLogo.png/")}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'

    }
})