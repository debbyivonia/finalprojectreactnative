import React, { useEffect } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, TextInput, StyleSheet } from "react-native"
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
export default function Home() {
    return (
        <View style={styles.container}>

            <View style={{ alignSelf: "flex-start", marginLeft: 10, flexDirection: 'row' }}>
                <View style={styles.header}>
                    <View style={{ alignSelf: "flex-start", marginLeft: 10, flexDirection: 'row' }}>
                        <View style={{ alignSelf: "flex-start", marginLeft: 5, marginTop: 5, opacity: 0.50 }}>
                            <FontAwesome name="search" size={24} color="black" />
                        </View>
                        <View style={{ alignSelf: "flex-start", marginLeft: 1, marginTop: 1, opacity: 0.50 }}>
                            <Text style={styles.text}>Searching...</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.icon1}>
                    <AntDesign name="heart" size={24} color="#C51D1D" />
                </View>
                <View style={styles.icon1}>
                    <MaterialCommunityIcons name="message-processing" size={26} color="#C51D1D" />
                </View>
                <View style={styles.icon1}>
                    <FontAwesome5 name="shopping-cart" size={24} color="#C51D1D" />
                </View>
            </View>
            <View style={styles.header2}>
                <View style={styles.header3}>
                    <View style={{ alignSelf: "flex-start", marginLeft: 10, flexDirection: 'row' }}>
                        <View style={{ alignSelf: "flex-start", marginLeft: 5, marginTop: 5, opacity: 0.50 }}>
                            <FontAwesome5 name="wallet" size={24} color="black" />
                        </View>
                        <View style={{ alignSelf: "flex-start", marginLeft: 1 }}>
                            <Text style={styles.text2}>Rp 1.000.000</Text>
                            <Text style={styles.text3}>Top-Up</Text>
                        </View>
                    </View>
                </View>
                <View style={{ alignSelf: "flex-start", marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={styles.text4}>Popular Brand</Text>
                    <View style={{ alignSelf: "flex-start", marginLeft: 132, marginTop: 12, opacity: 0.50 }}>
                        <MaterialIcons name="arrow-forward-ios" size={20} color="#ffffff" />
                    </View>
                </View>
                <View style={{ alignSelf: "flex-start", marginLeft: 5, flexDirection: 'row' }}>
                    <Image
                        style={styles.logo}
                        source={require("./images/nike.png/")} />
                    <Image
                        style={styles.logo}
                        source={require("./images/vans.jpg/")} />
                    <Image
                        style={styles.logo}
                        source={require("./images/converse.png/")} />
                </View>
            </View>
            <View style ={styles.header4}>
            <View style={{ alignSelf: "flex-start", marginLeft: 5, flexDirection: 'row' }}>
                    <Text style={styles.text4}>New Arrival </Text>
                    <Text style={styles.text5}>All</Text>
                    <View style={{ alignSelf: "flex-start", marginLeft: 5, marginTop: 14, opacity: 0.50 }}>
                        <MaterialIcons name="arrow-forward-ios" size={20} color="#ffffff" />
                    </View>
                </View>
                <View style={{ alignSelf: "flex-start", marginLeft: 5, flexDirection: 'row' }}>
                    <Image
                        style={styles.sepatu}
                        source={require("./images/kuning.jpg/")} />
                    <Image
                        style={styles.sepatu}
                        source={require("./images/hitam.jpg/")} />
                    
                </View>

            </View>
            <View style={styles.header5}>
            <View style={{ alignSelf: "flex-start", marginLeft: 5, flexDirection: 'row' }}>
            <View style={styles.icon2}>
            <FontAwesome5 name="store" size={30} color="#C51D1D" />
            <Text style={styles.text6}>Discover </Text>
                </View>
                <View style={styles.icon2}>
                <FontAwesome name="list-ul" size={32} color="#C51D1D" />
                <Text style={styles.text6}>Feeds</Text>
                </View>
                <View style={styles.icon2}>
                <FontAwesome5 name="gift" size={30} color="#C51D1D" />
                <Text style={styles.text6}>For You</Text>
                </View>
                <View style={styles.icon2}>
                <FontAwesome5 name="bell" size={30} color="#C51D1D" />
                <Text style={styles.text6}>Notification</Text>
                </View>
                <View style={styles.icon2}>
                <FontAwesome5 name="user" size={30} color="#C51D1D" />
                <Text style={styles.text6}>Profile</Text>
                </View>
                    
                </View>


            </View>



        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF9F9',


    },
    header: {
        backgroundColor: '#FFFDFD',
        borderColor: '#000000',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        height: 40, width: 200,
        marginTop: 50,
        marginLeft: 20,
        borderWidth: 1
    },
    text: {
        alignItems: 'flex-start',
        fontSize: 18,
        marginLeft: 20,
        marginTop: 5,
        opacity: 0.70

    },
    icon1: {
        alignSelf: "flex-start",
        marginLeft: 10,
        marginTop: 60,
        opacity: 0.50
    },
    header2: {
        backgroundColor: '#B92F2F',
        borderColor: '#000000',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        height: 200, width: 315,
        marginTop: 35,
        marginLeft: 20,
        borderWidth: 1

    },
    header3: {
        backgroundColor: '#FFFDFD',
        borderColor: '#000000',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        height: 40, width: 275,
        marginTop: 10,
        marginLeft: 20,
        borderWidth: 1
    },
    text2: {
        alignItems: 'flex-start',
        fontSize: 14,
        marginLeft: 20,
        opacity: 0.70
    },
    text3: {
        alignItems: 'flex-start',
        fontSize: 14,
        marginLeft: 40,
        color: '#C51D1D'
    },
    text4: {
        alignSelf: "flex-start",
        marginLeft: 10,
        marginTop: 7,
        fontSize: 24,
        color: "#FFFFFF"
    },
    logo: {
        height: 70,
        width: 90,
        marginLeft: 7,
        marginTop: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    header4:{
        backgroundColor: '#B92F2F',
        borderColor: '#000000',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        height: 200, width: 315,
        marginTop: 40,
        marginLeft: 20,
        borderWidth: 1
    },
    text5:{
        alignSelf: "flex-start",
        marginLeft: 120,
        marginTop: 7,
        fontSize: 24,
        color: "#FFFFFF"
    },
    sepatu:{
        height: 90,
        width: 120,
        marginLeft: 5,
        marginTop: 15,
        marginRight:10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    header5:{
        backgroundColor: '#D8D5D5',
        borderColor: '#D8D5D5',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        height: 80, width: 360,
        marginTop: 86,
        borderWidth: 1,

    },
    icon2:{
        alignSelf: "flex-start",
        marginLeft:10,
        marginRight:20,
        marginTop: 20,
        opacity: 0.50

    },
    text6:{
        alignSelf: "flex-start",
        marginLeft: 1,
        marginTop: 5,
        fontSize: 12,
        color: "#000000"

    }

})